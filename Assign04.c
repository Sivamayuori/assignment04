//This is a program to convert temperature from Celsius to Farenheit
#include <stdio.h>
int main (){
	float celsius, farenheit;

	//Entering the temperature in celsius by the user
	printf ("Please enter the temperature in Celsius: \n");

	//Reading the entered temperature
	scanf ("%f", &celsius);

	farenheit = (celsius*9.0/5.0)+32;

	//Displaying the temperature in farenheit
	printf ("The temperature in Farenheit is: %.2f \n", farenheit);

	return 0;
}
