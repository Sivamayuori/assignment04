//This is a program to demonstrate bitwise operators
#include <stdio.h>
int main (){
	int A=10, B=15;

	//Displaying the values of A and B
	printf ("A = %d, B = %d \n",A,B);

	printf ("A & B = %d \n", A&B);
	printf ("A ^ B = %d \n", A^B);
	printf ("~A = %d \n", ~A);
	printf ("A<<3 = %d \n", A<<3);
	printf ("B>>3 = %d \n", B>>3);

	return 0;
}
