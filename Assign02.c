//This is a program to find the volume of a cone
#include <stdio.h>
int main (){
	int radius, height;
	float volume; 
	

	//Entering the radius of the cone by the user
	printf ("Please enter the radius of the cone: \n");
	//Reading the entered radius
	scanf ("%d", &radius);

	//Entering the height of the cone by the user
	printf ("Please enter the height of the cone: \n");
	//Reading the entered height 
	scanf ("%d", &height);


	volume = (22/7.0)*radius*radius*(height/3.0);

	//Displaying the volume of the cone
	printf ("The volume of the cone is: %.2f \n", volume);

	return 0;
}
