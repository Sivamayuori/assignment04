//This is a program to swap two numbers without using
//a temporary third variable
#include <stdio.h>
int main (){
	int x,y;

	//Entering the value of first number
	printf ("Please enter the first number: \n");
	//Reading the entered number
	scanf ("%d", &x);

	//Entering the value of second number
	printf ("Please enter the second number: \n");
	//Reading the entered number
	scanf ("%d", &y);

	//Displaying both numbers
	printf ("The first number is %d and the second number is %d before swapping \n",x,y);

	x = x + y;
	y = x - y;
	x = x - y;

	//Displaying the swapped numbers
	printf ("The first number is %d and the second number is %d after swapping \n",x,y);

	return 0;
}

