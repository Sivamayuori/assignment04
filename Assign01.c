//This is a porgram to calculate the sum and average of 3 integers
#include <stdio.h>
int main(){
	int a,b,c,sum=0;
	float avg;

	//Entering an integer by the user
	printf ("Please enter the first integer: \n");
	//Reading the entered integer
	scanf ("%d", &a);

	//Entering an integer by the user
	printf ("Please enter the second integer: \n");
	//Reading the entered integer
	scanf ("%d", &b);

	//Entering an integer by the user
	printf ("Please enter the third integer: \n");
	//Reading the entered integer
	scanf ("%d", &c);

	//Displaying all three integers
	printf ("The three integers you entered are: %d %d and %d \n", a,b,c);

	sum = a + b + c;
	avg = sum / 3.0;

	//Displaying the sum and average of the 3 integers
	printf ("The sum is: %d \n", sum);

	printf ("The average is: %.2f \n", avg);

	return 0;
}


